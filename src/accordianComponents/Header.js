import React from 'react'
import clock from './img/clock.png'

export const Header = () => {
    return (
        <div className='header'>
            <img  src={clock} alt = "Clock-Icon"/>
            <h1>Subscription</h1>
        </div>
    )
}

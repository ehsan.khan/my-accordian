import React, { useState } from "react"


export const Accordian = ({title,content}) => {
    const [clicked, setClicked] = useState(false);
    return (
        <div className='accordianBox'>
            <div className="title">
            <h1 className="accordianTitle">{title}</h1>
            <div className="button" onClick={()=>setClicked(!clicked)}>{clicked ? '-':'+'}</div>
            </div>
            {
                clicked && <p className="content">{content}</p>
            }
            
        </div>
    )
}

import './App.css';
import {Header} from'./accordianComponents/Header';
import {Accordian} from './accordianComponents/Accordian'

function App() {
const data = [
  {
    title:'Can I cancel my subscription at anytime?',
    content:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ullamcorper quis felis eu hendrerit. Ut id sem et mauris elementum ullamcorper in a velit. Sed vel aliquet nisl. Curabitur dapibus lacus urna, non interdum erat gravida id. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam pellentesque nec metus nec volutpat. Ut est est, interdum nec interdum et, molestie vel nulla. Suspendisse varius non libero sed fermentum. Integer ac sapien magna.',
  },
  {
    'title':'Can I change my plan later on?',
    'content':'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ullamcorper quis felis eu hendrerit. Ut id sem et mauris elementum ullamcorper in a velit. Sed vel aliquet nisl. Curabitur dapibus lacus urna, non interdum erat gravida id. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam pellentesque nec metus nec volutpat. Ut est est, interdum nec interdum et, molestie vel nulla. Suspendisse varius non libero sed fermentum. Integer ac sapien magna.',
  },
  {
    'title':'Will you renew my subscription automatically?',
    'content':'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ullamcorper quis felis eu hendrerit. Ut id sem et mauris elementum ullamcorper in a velit. Sed vel aliquet nisl. Curabitur dapibus lacus urna, non interdum erat gravida id. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam pellentesque nec metus nec volutpat. Ut est est, interdum nec interdum et, molestie vel nulla. Suspendisse varius non libero sed fermentum. Integer ac sapien magna.',
  },
  {
    'title':'Do you offer any discounts?',
    'content':'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ullamcorper quis felis eu hendrerit. Ut id sem et mauris elementum ullamcorper in a velit. Sed vel aliquet nisl. Curabitur dapibus lacus urna, non interdum erat gravida id. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam pellentesque nec metus nec volutpat. Ut est est, interdum nec interdum et, molestie vel nulla. Suspendisse varius non libero sed fermentum. Integer ac sapien magna.',
  },
  {
    'title':'Can I request a refund?',
    'content':'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ullamcorper quis felis eu hendrerit. Ut id sem et mauris elementum ullamcorper in a velit. Sed vel aliquet nisl. Curabitur dapibus lacus urna, non interdum erat gravida id. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam pellentesque nec metus nec volutpat. Ut est est, interdum nec interdum et, molestie vel nulla. Suspendisse varius non libero sed fermentum. Integer ac sapien magna.',
  }
];

  return (
    <div className="App">
      <h1 className='mainHead'>Frequently Asked Questions</h1>
      <div className='secondary'>
      <Header/>
      
      {data.map((item)=>{
        return(
          <Accordian
      title ={item.title}
      content ={item.content}/>
        )
      })}
      </div>
    </div>
  );
}

export default App;
